package awsasg

import (
	"errors"
	"fmt"
	"time"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/awserr"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/autoscaling"
	"github.com/aws/aws-sdk-go/service/ec2"
)

// LCBody ...
type LCBody struct {
	KeyName        string
	SecurityGroups []*string
	InstanceType   string
	UserData       string
}

// GetInstanceIDFromASG ...
// Return: InstanceId, LC NAME, error
func GetInstanceIDFromASG(asgName string) (string, string, error) {
	svc := autoscaling.New(session.New())
	input := &autoscaling.DescribeAutoScalingGroupsInput{
		AutoScalingGroupNames: []*string{
			aws.String(asgName),
		},
	}

	result, err := svc.DescribeAutoScalingGroups(input)
	if err != nil {
		if aerr, ok := err.(awserr.Error); ok {
			switch aerr.Code() {
			case autoscaling.ErrCodeInvalidNextToken:
				fmt.Println(autoscaling.ErrCodeInvalidNextToken, aerr.Error())
			case autoscaling.ErrCodeResourceContentionFault:
				fmt.Println(autoscaling.ErrCodeResourceContentionFault, aerr.Error())
			default:
				fmt.Println(aerr.Error())
			}
		} else {
			// Print the error, cast err to awserr.Error to get the Code and
			// Message from an error.
			fmt.Println(err.Error())
		}
		return "", "", err
	}

	// fmt.Println(result)
	fmt.Println(len(result.AutoScalingGroups))
	if len(result.AutoScalingGroups) > 0 {
		if len(result.AutoScalingGroups[0].Instances) > 0 {
			fmt.Println(*result.AutoScalingGroups[0].Instances[0].InstanceId)
		} else {
			return "", "", errors.New("Don't have instance in this ASG")
		}
	} else {
		return "", "", errors.New("Don't have Auto Scaling Groups with this name")
	}
	return *result.AutoScalingGroups[0].Instances[0].InstanceId, *result.AutoScalingGroups[0].LaunchConfigurationName, nil
}

// CreateImagaFromInstanceID ...
// Return: ImageID, error
func CreateImagaFromInstanceID(InstanceID string) (string, error) {
	location, _ := time.LoadLocation("Asia/Ho_Chi_Minh")
	t := time.Now().In(location)
	prefix := fmt.Sprintf("%d-%02d-%02dT%02d-%02d-%02d", t.Year(), t.Month(), t.Day(), t.Hour(), t.Minute(), t.Second())
	fmt.Println(prefix)
	svc := ec2.New(session.New())
	input := &ec2.CreateImageInput{
		Description: aws.String("An AMI for my server"),
		InstanceId:  aws.String(InstanceID),
		Name:        aws.String(prefix + "_test-ami"),
	}

	result, err := svc.CreateImage(input)
	if err != nil {
		if aerr, ok := err.(awserr.Error); ok {
			switch aerr.Code() {
			default:
				fmt.Println(aerr.Error())
			}
		} else {
			// Print the error, cast err to awserr.Error to get the Code and
			// Message from an error.
			fmt.Println(err.Error())
		}
		return "", err
	}

	fmt.Println(*result.ImageId)
	return *result.ImageId, nil
}

// GetConfigLCCurrent ...
// Return struct LCBody, error
func GetConfigLCCurrent(OldLCName string) (LCBody, error) {
	LCBodyOrigin := LCBody{}
	svc := autoscaling.New(session.New())
	input := &autoscaling.DescribeLaunchConfigurationsInput{
		LaunchConfigurationNames: []*string{
			aws.String(OldLCName),
		},
	}

	result, err := svc.DescribeLaunchConfigurations(input)
	if err != nil {
		if aerr, ok := err.(awserr.Error); ok {
			switch aerr.Code() {
			case autoscaling.ErrCodeInvalidNextToken:
				fmt.Println(autoscaling.ErrCodeInvalidNextToken, aerr.Error())
			case autoscaling.ErrCodeResourceContentionFault:
				fmt.Println(autoscaling.ErrCodeResourceContentionFault, aerr.Error())
			default:
				fmt.Println(aerr.Error())
			}
		} else {
			// Print the error, cast err to awserr.Error to get the Code and
			// Message from an error.
			fmt.Println(err.Error())
		}
		return LCBodyOrigin, err
	}
	fmt.Println(result)
	if len(result.LaunchConfigurations) > 0 {
		LCBodyOrigin.KeyName = *result.LaunchConfigurations[0].KeyName
		LCBodyOrigin.InstanceType = *result.LaunchConfigurations[0].InstanceType
		LCBodyOrigin.UserData = *result.LaunchConfigurations[0].UserData
		if len(result.LaunchConfigurations[0].SecurityGroups) > 0 {
			LCBodyOrigin.SecurityGroups = result.LaunchConfigurations[0].SecurityGroups
		} else {
			return LCBodyOrigin, errors.New("Don't have Security Groups in this ASG")
		}
	} else {
		return LCBodyOrigin, errors.New("Don't have LC with this name")
	}
	return LCBodyOrigin, nil
}

// CreateNewLC ...
// Return newLCName, error
func CreateNewLC(LCBodyOrigin LCBody, newAMI string) (string, error) {
	location, _ := time.LoadLocation("Asia/Ho_Chi_Minh")
	t := time.Now().In(location)
	prefix := fmt.Sprintf("%d-%02d-%02dT%02d-%02d-%02d", t.Year(), t.Month(), t.Day(), t.Hour(), t.Minute(), t.Second())
	svc := autoscaling.New(session.New())
	input := &autoscaling.CreateLaunchConfigurationInput{
		ImageId:                 aws.String(newAMI),
		InstanceType:            aws.String(LCBodyOrigin.InstanceType),
		LaunchConfigurationName: aws.String(prefix + "-my-launch-config"),
		// SecurityGroups: []*string{
		// 	aws.String(LCBodyOrigin.SecurityGroups),
		// },
		SecurityGroups: LCBodyOrigin.SecurityGroups,
		KeyName:        aws.String(LCBodyOrigin.KeyName),
		UserData:       aws.String(LCBodyOrigin.UserData),
	}

	result, err := svc.CreateLaunchConfiguration(input)
	if err != nil {
		if aerr, ok := err.(awserr.Error); ok {
			switch aerr.Code() {
			case autoscaling.ErrCodeAlreadyExistsFault:
				fmt.Println(autoscaling.ErrCodeAlreadyExistsFault, aerr.Error())
			case autoscaling.ErrCodeLimitExceededFault:
				fmt.Println(autoscaling.ErrCodeLimitExceededFault, aerr.Error())
			case autoscaling.ErrCodeResourceContentionFault:
				fmt.Println(autoscaling.ErrCodeResourceContentionFault, aerr.Error())
			default:
				fmt.Println(aerr.Error())
			}
		} else {
			// Print the error, cast err to awserr.Error to get the Code and
			// Message from an error.
			fmt.Println(err.Error())
		}
		return "", err
	}

	fmt.Println(result)
	newLCName := prefix + "-my-launch-config"
	return newLCName, nil
}

// UpdateASG ...
func UpdateASG(asgName string, newLCName string) error {
	svc := autoscaling.New(session.New())
	input := &autoscaling.UpdateAutoScalingGroupInput{
		AutoScalingGroupName:    aws.String(asgName),
		LaunchConfigurationName: aws.String(newLCName),
	}

	result, err := svc.UpdateAutoScalingGroup(input)
	if err != nil {
		if aerr, ok := err.(awserr.Error); ok {
			switch aerr.Code() {
			case autoscaling.ErrCodeScalingActivityInProgressFault:
				fmt.Println(autoscaling.ErrCodeScalingActivityInProgressFault, aerr.Error())
			case autoscaling.ErrCodeResourceContentionFault:
				fmt.Println(autoscaling.ErrCodeResourceContentionFault, aerr.Error())
			case autoscaling.ErrCodeServiceLinkedRoleFailure:
				fmt.Println(autoscaling.ErrCodeServiceLinkedRoleFailure, aerr.Error())
			default:
				fmt.Println(aerr.Error())
			}
		} else {
			// Print the error, cast err to awserr.Error to get the Code and
			// Message from an error.
			fmt.Println(err.Error())
		}
		return err
	}

	fmt.Println(result)
	return nil
}

// UpdateASGModels ...
// Return newLCName, error
func UpdateASGModels(asgName string) (string, error) {
	InstanceID, OldLCName, err := GetInstanceIDFromASG(asgName)
	if err != nil {
		return "", err
	}

	NewAMI, imageErr := CreateImagaFromInstanceID(InstanceID)
	if imageErr != nil {
		return "", imageErr
	}
	fmt.Println(OldLCName)
	LCBodyOrigin, lcErr := GetConfigLCCurrent(OldLCName)
	if lcErr != nil {
		return "", lcErr
	}
	fmt.Printf("%v", LCBodyOrigin)
	newLCName, newlcErr := CreateNewLC(LCBodyOrigin, NewAMI)
	if newlcErr != nil {
		return "", newlcErr
	}
	fmt.Println(newLCName)
	UpdateErr := UpdateASG(asgName, newLCName)
	if UpdateErr != nil {
		return "", UpdateErr
	}
	return newLCName, nil
}
