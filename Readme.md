```txt
Author: Tu Giang Hai Dang
Role: DevOps Engineer
```

## Chuẩn bị

Các ENV truyền vào khi tiến hành deploy

AWS_REGION=us-west-2

AWS_ACCESS_KEY_ID=<key_id>

AWS_SECRET_ACCESS_KEY=<secret_access_key>

## Build and push

```bash
TAG=v2
docker build -t api-update-asg:${TAG} .
docker tag api-update-asg:${TAG} dangtgh/api-update-asg:${TAG}
docker push dangtgh/api-update-asg:${TAG}
```

## Call api

```bash
curl -X GET http://<domain>:5000/updateasg?asgname=<asgName>
```

## Workflow CI/CD build and deploy on k8s

Flow dưới đây mô tả các bước cần làm để build và deploy code lên k8s theo cách của bản thân (Phần deploy sử dụng strategy đơn giản nhất là Rollout Deployment)

1. **Các công cụ sử dụng**

- **Pha build:**

Gitlab để chứa source code

gitlab-ci, gitlab-runner () để sử dụng CI cho việc build image

Dockerfile mô tả các bước build 1 image

skaffold là 1 tool hỗ trợ việc sử dụng kubernetes để build image và push image lên registry

- **Pha deloy:**

Helm là 1 tool dùng để  cài đặt, quản lý các ứng dụng deploy lên k8s

Helm template là mẫu helm cho ứng dựng được mình định nghĩa để hỗ trợ deploy image lên k8s

File value custom của helm cho application sẽ đặt ở repository riêng trên git (trong đó define value như image, port, ingress, secret, env,... cho ứng dụng)

Dùng Spinnaker / ArgoCD hoặc một tool CD nào khác có hỗ trợ plugin helm để parse manifest deploy lên k8s

2. **Quy trình build và deploy CI/CD lên k8s**

- Step 1: gitlab-ci sẽ trigger khi có code mới push lên repository sẽ tiến hành gọi gitlab-runner để thực thi

- Step 2: gitlab-runner sẽ run theo mô tả trong gitlab-ci và gọi skaffold để tiến hành build và push image

- Step 3: skaffold sẽ tiến hành build image dựa vào Dockerfile và tiến hành push image về registry mình chỉ định

- Step 4: Bên phía CD, Spinnaker / ArgoCD tiến hành trigger nếu có image mới hoặc trigger theo tag latest thì run CD

- Step 5: Các CD tools sẽ kéo helm-template mà mình define về và đọc vào file value custom sau đó tiến hành parse ra manifest

- Step 6: Các CD tools tiến hành deploy file manifest đã tạo lên trên k8s

## Reference

Authentication: https://docs.aws.amazon.com/sdk-for-go/v1/developer-guide/configuring-sdk.html

DescribeASG: https://docs.aws.amazon.com/sdk-for-go/api/service/autoscaling/#AutoScaling.DescribeAutoScalingGroups

CreateImage: https://docs.aws.amazon.com/sdk-for-go/api/service/ec2/#example_EC2_CreateImage_shared00

CreateLC: https://docs.aws.amazon.com/sdk-for-go/api/service/autoscaling/#AutoScaling.CreateLaunchConfiguration

UpdateASG: https://docs.aws.amazon.com/sdk-for-go/api/service/autoscaling/#AutoScaling.UpdateAutoScalingGroup

DescribeLC: https://docs.aws.amazon.com/sdk-for-go/api/service/autoscaling/#AutoScaling.DescribeLaunchConfigurations