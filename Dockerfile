FROM golang:1.14.2-alpine3.11

WORKDIR /app

COPY . .

RUN go build -o api-update-asg main.go

CMD ./api-update-asg
