package main

import (
	"fmt"
	"net/http"

	"api-update-asg/awsasg"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
)

func updateAsg(c echo.Context) error {
	asgName := c.QueryParam("asgname")
	newLCName, err := awsasg.UpdateASGModels(asgName)
	if err != nil {
		return c.JSON(http.StatusBadRequest, echo.Map{"result": false, "LC": err.Error()})
	}
	return c.JSON(http.StatusOK, echo.Map{"result": true, "LC": newLCName})
}

func main() {
	e := echo.New()
	e.Use(middleware.Logger())
	e.Use(middleware.Recover())
	e.GET("/", func(c echo.Context) error {
		fmt.Println(c.Request().Header)
		return c.JSON(http.StatusOK, echo.Map{"code": 200, "result": "success v1"})
	})
	// /updateasg?asgname=abc
	e.GET("/updateasg", updateAsg)
	e.Logger.Fatal(e.Start(":5000"))
}
