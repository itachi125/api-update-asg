module api-update-asg

go 1.14

require (
	github.com/aws/aws-sdk-go v1.38.52
	github.com/labstack/echo/v4 v4.3.0 // indirect
)
